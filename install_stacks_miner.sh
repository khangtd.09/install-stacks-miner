#!/usr/bin/env bash

bash pre_setup.sh
bash install_stacks_deps.sh
bash install_stacks.sh
bash install_supervisord.sh
