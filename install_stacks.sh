#!/usr/bin/env bash
source ~/.zshrc

nvm install 14.15.0

nvm alias default 14.15.0

# install stacks blockchain
git clone https://github.com/blockstack/stacks-blockchain.git
cd stacks-blockchain
cargo build --workspace --release --bin stacks-node
cp target/release/stacks-node $HOME/.cargo/bin
cd ..

# install mining server
git clone https://github.com/Daemon-Technologies/Mining-Local-Server.git
cd Mining-Local-Server
yarn install
cd ..

# install bot server
git clone https://github.com/Daemon-Technologies/Mining-Bot.git
cd Mining-Bot
yarn install
cd ..
