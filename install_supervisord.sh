#!/usr/bin/env bash

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install supervisor

echo_supervisord_conf > supervisord.conf

cat <<EOF | tee --append supervisord.conf
[program:mining-server]
command=/usr/bin/yarn --cwd /root/Mining-Local-Server start

[program:mining-bot]
command=/usr/bin/yarn --cwd /root/Mining-Bot start
EOF

supervisord -c supervisord.conf
